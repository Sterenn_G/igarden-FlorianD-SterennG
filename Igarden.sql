-- Adminer 4.8.1 MySQL 8.0.28 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `mydb`;
CREATE DATABASE `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `mydb`;

DROP TABLE IF EXISTS `Classification`;
CREATE TABLE `Classification` (
  `idClassification` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idClassification`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Cycle`;
CREATE TABLE `Cycle` (
  `idCycle` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idCycle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Etat`;
CREATE TABLE `Etat` (
  `idEtat` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  `Statut` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idEtat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Famille`;
CREATE TABLE `Famille` (
  `idFamille` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `Cultivar` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idFamille`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Genre`;
CREATE TABLE `Genre` (
  `idGenre` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idGenre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Graine`;
CREATE TABLE `Graine` (
  `idGraine` int NOT NULL,
  `Icone` varchar(45) DEFAULT NULL,
  `Photo` varchar(45) DEFAULT NULL,
  `Nom` varchar(45) NOT NULL,
  `Nom latin` varchar(45) DEFAULT NULL,
  `Classification` varchar(45) NOT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `Variété` varchar(45) DEFAULT NULL,
  `Hauteur max` varchar(45) DEFAULT NULL,
  `Jardin` varchar(45) DEFAULT NULL,
  `Famille_idFamille` int NOT NULL,
  `Genre_idGenre` int NOT NULL,
  `Plante_idPlante` int NOT NULL,
  PRIMARY KEY (`idGraine`,`Famille_idFamille`,`Genre_idGenre`,`Plante_idPlante`),
  KEY `fk_Graine_Famille1_idx` (`Famille_idFamille`),
  KEY `fk_Graine_Genre1_idx` (`Genre_idGenre`),
  KEY `fk_Graine_Plante1_idx` (`Plante_idPlante`),
  CONSTRAINT `fk_Graine_Famille1` FOREIGN KEY (`Famille_idFamille`) REFERENCES `Famille` (`idFamille`),
  CONSTRAINT `fk_Graine_Genre1` FOREIGN KEY (`Genre_idGenre`) REFERENCES `Genre` (`idGenre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Jardin`;
CREATE TABLE `Jardin` (
  `idJardin` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  `User` varchar(45) DEFAULT NULL,
  `CP` varchar(45) DEFAULT NULL,
  `Rusticité` varchar(45) DEFAULT NULL,
  `Nb de zones` int DEFAULT NULL,
  `Zone_idZone` int NOT NULL,
  PRIMARY KEY (`idJardin`,`Zone_idZone`),
  KEY `fk_Jardin_Zone1_idx` (`Zone_idZone`),
  CONSTRAINT `fk_Jardin_Zone1` FOREIGN KEY (`Zone_idZone`) REFERENCES `Zone` (`idZone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Photo`;
CREATE TABLE `Photo` (
  `idPhoto` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idPhoto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Plante`;
CREATE TABLE `Plante` (
  `idPlante` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(45) DEFAULT NULL,
  `Date semis` date DEFAULT NULL,
  `Date repiquage` date DEFAULT NULL,
  `Date floraison` date DEFAULT NULL,
  `Date de récolte` date DEFAULT NULL,
  `Quantité` decimal(10,0) DEFAULT NULL,
  `Etat` varchar(45) DEFAULT NULL,
  `Cycle` varchar(45) DEFAULT NULL,
  `Graine_idGraine` int NOT NULL,
  PRIMARY KEY (`idPlante`),
  UNIQUE KEY `idPlante_UNIQUE` (`idPlante`),
  KEY `Graine_idGraine` (`Graine_idGraine`),
  CONSTRAINT `Plante_ibfk_1` FOREIGN KEY (`Graine_idGraine`) REFERENCES `Plante` (`idPlante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Plante_has_Etat`;
CREATE TABLE `Plante_has_Etat` (
  `Plante_idPlante` int NOT NULL,
  `Etat_idEtat` int NOT NULL,
  `Statut_idStatut` int NOT NULL,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Plante_idPlante`,`Etat_idEtat`,`Statut_idStatut`),
  KEY `fk_Plante_has_Etat_Etat1_idx` (`Etat_idEtat`),
  KEY `fk_Plante_has_Etat_Plante1_idx` (`Plante_idPlante`),
  KEY `fk_Plante_has_Etat_Statut1_idx` (`Statut_idStatut`),
  CONSTRAINT `fk_Plante_has_Etat_Etat1` FOREIGN KEY (`Etat_idEtat`) REFERENCES `Etat` (`idEtat`),
  CONSTRAINT `fk_Plante_has_Etat_Plante1` FOREIGN KEY (`Plante_idPlante`) REFERENCES `Plante` (`idPlante`),
  CONSTRAINT `fk_Plante_has_Etat_Statut1` FOREIGN KEY (`Statut_idStatut`) REFERENCES `Statut` (`idStatut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Plante_has_Zone`;
CREATE TABLE `Plante_has_Zone` (
  `Plante_idPlante` int NOT NULL,
  `Zone_idZone` int NOT NULL,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Plante_idPlante`,`Zone_idZone`),
  KEY `fk_Plante_has_Zone_Zone1_idx` (`Zone_idZone`),
  KEY `fk_Plante_has_Zone_Plante1_idx` (`Plante_idPlante`),
  CONSTRAINT `fk_Plante_has_Zone_Plante1` FOREIGN KEY (`Plante_idPlante`) REFERENCES `Plante` (`idPlante`),
  CONSTRAINT `fk_Plante_has_Zone_Zone1` FOREIGN KEY (`Zone_idZone`) REFERENCES `Zone` (`idZone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Serre`;
CREATE TABLE `Serre` (
  `idSerre` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idSerre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Statut`;
CREATE TABLE `Statut` (
  `idStatut` int NOT NULL,
  `Label` int DEFAULT NULL,
  PRIMARY KEY (`idStatut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Type de sol`;
CREATE TABLE `Type de sol` (
  `idType de sol` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idType de sol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `Pseudo` int NOT NULL,
  `E-mail` int DEFAULT NULL,
  `Mdp` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `User_has_Jardin`;
CREATE TABLE `User_has_Jardin` (
  `User_Pseudo` int NOT NULL,
  `Jardin_idJardin` int NOT NULL,
  PRIMARY KEY (`User_Pseudo`,`Jardin_idJardin`),
  KEY `fk_User_has_Jardin_Jardin1_idx` (`Jardin_idJardin`),
  KEY `fk_User_has_Jardin_User1_idx` (`User_Pseudo`),
  CONSTRAINT `fk_User_has_Jardin_Jardin1` FOREIGN KEY (`Jardin_idJardin`) REFERENCES `Jardin` (`idJardin`),
  CONSTRAINT `fk_User_has_Jardin_User1` FOREIGN KEY (`User_Pseudo`) REFERENCES `User` (`Pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Zone`;
CREATE TABLE `Zone` (
  `idZone` int NOT NULL,
  `idJardin` int DEFAULT NULL,
  `Code` varchar(45) DEFAULT NULL,
  `Label` varchar(45) DEFAULT NULL,
  `Descrption` varchar(45) DEFAULT NULL,
  `Zone spéciale_idZone spéciale` int NOT NULL,
  `Type de sol_idType de sol` int NOT NULL,
  `Serre_idSerre` int NOT NULL,
  PRIMARY KEY (`idZone`,`Zone spéciale_idZone spéciale`,`Type de sol_idType de sol`,`Serre_idSerre`),
  KEY `fk_Zone_Zone spéciale1_idx` (`Zone spéciale_idZone spéciale`),
  KEY `fk_Zone_Type de sol1_idx` (`Type de sol_idType de sol`),
  KEY `fk_Zone_Serre1_idx` (`Serre_idSerre`),
  CONSTRAINT `fk_Zone_Serre1` FOREIGN KEY (`Serre_idSerre`) REFERENCES `Serre` (`idSerre`),
  CONSTRAINT `fk_Zone_Type de sol1` FOREIGN KEY (`Type de sol_idType de sol`) REFERENCES `Type de sol` (`idType de sol`),
  CONSTRAINT `fk_Zone_Zone spéciale1` FOREIGN KEY (`Zone spéciale_idZone spéciale`) REFERENCES `Zone spéciale` (`idZone spéciale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP TABLE IF EXISTS `Zone spéciale`;
CREATE TABLE `Zone spéciale` (
  `idZone spéciale` int NOT NULL,
  `Label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idZone spéciale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


DROP VIEW IF EXISTS `liste_de_plante`;
CREATE TABLE `liste_de_plante` (`Nom` varchar(45), `Label` varchar(45));


DROP TABLE IF EXISTS `liste_de_plante`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `liste_de_plante` AS select `Plante`.`Nom` AS `Nom`,`Zone`.`Label` AS `Label` from (`Plante` join (`Zone` join `Jardin` on((`Zone`.`idZone` = `Jardin`.`Zone_idZone`))));

-- 2022-04-20 09:03:50
