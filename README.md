# Igarden FlorianD SterennG

La solution i-garden est une application numérique fictive permettant de gérer des plantes.
Cette application, destinée aux particuliers novices ou non dans le jardinage permet à son utilisateur de pouvoir gérer ses arrosages, entretiens, récoltes ..

## Pour commencer
Après avoir ouvert  le DBMS (Base de données manager) de votre choix, importer le fichier.sql.
Ce projet est un projet fictif réalisé dans le cadre d'un exercice de formation.
Dans ce dossier, vous retrouverez les différentes étapes du projet et les différents états de la BDD au cours des différents sprints.

###La dernière version a prendre en compte pour la notation se trouve dans le dossier "Livrable du 13/05"

## Livrable
A l'issue de la deadLine donnée par le client, vous trouverez les éléments suivants :
    - Une base de données fonctionnelles (préremplie de données fictives)
    - Des déclencheurs (historisation (états, zones, cycles végétatifs), la suppréssion d'éléments dans la base de données)
    - Des vues permetant aux utilisateurs de visualiser le résultat de requête prédéfinies telles que la liste des plantes, la liste des graines ...

## Reste à faire
Nous n'avons malheureusement pas pu aller au bout des demandes du client.
Parmis le reste à faire certaines vues et contraintes du sprint 2 qui seront implémentés plus tard en fonction des retours du clients sur cette première session de travail.
Un seul fichier SQL est a importer contrairement à la demande initiale, en l'état, nous préconnisons une sauvegarde préalable des données clients car l'avancement de notre livrable ne garantie pas la conservation des données pour l'instant.

## Auteurs
Projet réalisé par Florian D et Sterenn G

