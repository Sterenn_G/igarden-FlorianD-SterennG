-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 13 mai 2022 à 10:28
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `i-garden`
--

-- --------------------------------------------------------

--
-- Structure de la table `cycle_vegetatif`
--

DROP TABLE IF EXISTS `cycle_vegetatif`;
CREATE TABLE IF NOT EXISTS `cycle_vegetatif` (
  `id_cycle_vegetatif` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(15) NOT NULL,
  PRIMARY KEY (`id_cycle_vegetatif`),
  UNIQUE KEY `nom_UNIQUE` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cycle_vegetatif`
--

INSERT INTO `cycle_vegetatif` (`id_cycle_vegetatif`, `nom`) VALUES
(2, 'Croissance'),
(3, 'Floraison'),
(4, 'Fructification'),
(1, 'Germination'),
(5, 'Mort');

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

DROP TABLE IF EXISTS `etat`;
CREATE TABLE IF NOT EXISTS `etat` (
  `id_etat` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  PRIMARY KEY (`id_etat`),
  UNIQUE KEY `nom_UNIQUE` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etat`
--

INSERT INTO `etat` (`id_etat`, `nom`) VALUES
(2, 'Assoiffé'),
(1, 'Malade');

-- --------------------------------------------------------

--
-- Structure de la table `etat_plante`
--

DROP TABLE IF EXISTS `etat_plante`;
CREATE TABLE IF NOT EXISTS `etat_plante` (
  `id_plante` int(11) NOT NULL,
  `id_etat` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `status` enum('Actif','Passé','Traité') NOT NULL DEFAULT 'Actif',
  PRIMARY KEY (`id_plante`,`id_etat`),
  KEY `id_etat_idx` (`id_etat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etat_plante`
--

INSERT INTO `etat_plante` (`id_plante`, `id_etat`, `date`, `status`) VALUES
(6, 2, '2022-05-02 00:00:00', 'Actif');

--
-- Déclencheurs `etat_plante`
--
DROP TRIGGER IF EXISTS `Historiser_etat`;
DELIMITER $$
CREATE TRIGGER `Historiser_etat` AFTER UPDATE ON `etat_plante` FOR EACH ROW INSERT INTO historique_etat(date_etat, id_plante, id_etat) VALUES (NOW(),OLD.id_plante, OLD.id_etat)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `famille`
--

DROP TABLE IF EXISTS `famille`;
CREATE TABLE IF NOT EXISTS `famille` (
  `id_famille` int(11) NOT NULL AUTO_INCREMENT,
  `id_famille_parente` int(11) DEFAULT NULL,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id_famille`),
  KEY `nom_famille_UNIQUE` (`nom`),
  KEY `id_famille_parente_idx` (`id_famille_parente`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `famille`
--

INSERT INTO `famille` (`id_famille`, `id_famille_parente`, `nom`) VALUES
(1, NULL, 'Cucurbitaceae'),
(2, NULL, 'Alliaceae'),
(3, 1, 'Cucurbita'),
(4, 2, 'Allium'),
(5, 3, 'Cucurbita pepo'),
(6, 4, 'Bleu de Solaise');

-- --------------------------------------------------------

--
-- Structure de la table `graine`
--

DROP TABLE IF EXISTS `graine`;
CREATE TABLE IF NOT EXISTS `graine` (
  `id_graine` int(11) NOT NULL AUTO_INCREMENT,
  `id_famille` int(11) DEFAULT NULL,
  `nom` varchar(30) NOT NULL,
  `nom_latin` varchar(50) DEFAULT NULL,
  `classification` enum('Vivace','Annuelle','Bisannuelle') NOT NULL,
  `hauteur_max` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `icone` varchar(255) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`id_graine`),
  KEY `id_famille_idx` (`id_famille`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `graine`
--

INSERT INTO `graine` (`id_graine`, `id_famille`, `nom`, `nom_latin`, `classification`, `hauteur_max`, `photo`, `icone`, `description`) VALUES
(1, 6, 'Poireau bleu de solaise', 'Allium porrum', 'Vivace', 75, NULL, NULL, NULL),
(2, 5, 'Courge spaghetti', 'Cucurbita pepo', 'Annuelle', 50, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `historique_cycle`
--

DROP TABLE IF EXISTS `historique_cycle`;
CREATE TABLE IF NOT EXISTS `historique_cycle` (
  `id_cycle_vegetatif` int(11) NOT NULL,
  `id_plante` int(11) NOT NULL,
  `date_changement` datetime NOT NULL,
  PRIMARY KEY (`id_cycle_vegetatif`,`id_plante`),
  KEY `id_plante_idx` (`id_plante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `historique_cycle`
--

INSERT INTO `historique_cycle` (`id_cycle_vegetatif`, `id_plante`, `date_changement`) VALUES
(1, 6, '2022-04-02 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `historique_etat`
--

DROP TABLE IF EXISTS `historique_etat`;
CREATE TABLE IF NOT EXISTS `historique_etat` (
  `id_etat` int(11) NOT NULL,
  `id_plante` int(11) NOT NULL,
  `date_changement` datetime NOT NULL,
  `date` date DEFAULT NULL,
  `status` enum('Actif','Passé','Traité') NOT NULL,
  PRIMARY KEY (`id_etat`,`id_plante`,`date_changement`),
  KEY `id_plante_idx` (`id_plante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `historique_etat`
--

INSERT INTO `historique_etat` (`id_etat`, `id_plante`, `date_changement`, `date`, `status`) VALUES
(2, 6, '2022-04-27 00:00:00', '2022-04-26', 'Actif'),
(2, 6, '2022-05-01 00:00:00', '2022-04-27', 'Traité');

-- --------------------------------------------------------

--
-- Structure de la table `historique_zone`
--

DROP TABLE IF EXISTS `historique_zone`;
CREATE TABLE IF NOT EXISTS `historique_zone` (
  `id_plante` int(11) NOT NULL,
  `id_zone` int(11) NOT NULL,
  `date_changement` datetime NOT NULL,
  PRIMARY KEY (`id_plante`,`id_zone`),
  KEY `id_zone_idx` (`id_zone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `historique_zone`
--

INSERT INTO `historique_zone` (`id_plante`, `id_zone`, `date_changement`) VALUES
(6, 2, '2022-05-01 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `jardin`
--

DROP TABLE IF EXISTS `jardin`;
CREATE TABLE IF NOT EXISTS `jardin` (
  `id_jardin` int(11) NOT NULL AUTO_INCREMENT,
  `id_rusticite` int(11) DEFAULT NULL,
  `nom` varchar(30) NOT NULL,
  `code_postal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_jardin`),
  KEY `id_rusticite_idx` (`id_rusticite`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `jardin`
--

INSERT INTO `jardin` (`id_jardin`, `id_rusticite`, `nom`, `code_postal`) VALUES
(1, 11, 'Jardin de toto', 35000),
(2, 10, 'Jardin partagé', 35170);

-- --------------------------------------------------------

--
-- Structure de la table `jardin_utilisateur`
--

DROP TABLE IF EXISTS `jardin_utilisateur`;
CREATE TABLE IF NOT EXISTS `jardin_utilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `id_jardin` int(11) NOT NULL,
  PRIMARY KEY (`id_utilisateur`,`id_jardin`),
  KEY `id_jardin_idx` (`id_jardin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `jardin_utilisateur`
--

INSERT INTO `jardin_utilisateur` (`id_utilisateur`, `id_jardin`) VALUES
(1, 1),
(1, 2),
(2, 2);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `liste_des_plantes`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `liste_des_plantes`;
CREATE TABLE IF NOT EXISTS `liste_des_plantes` (
`Plante` varchar(30)
,`Zone` varchar(30)
,`Cycle_de_vie` varchar(15)
);

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `type` text NOT NULL,
  `description` text NOT NULL,
  `lien audio` blob NOT NULL,
  `date de création` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `plante`
--

DROP TABLE IF EXISTS `plante`;
CREATE TABLE IF NOT EXISTS `plante` (
  `id_plante` int(11) NOT NULL AUTO_INCREMENT,
  `id_graine` int(11) NOT NULL,
  `id_jardin` int(11) DEFAULT NULL,
  `id_zone` int(11) DEFAULT NULL,
  `id_cycle_vegetatif` int(11) NOT NULL,
  `date_semis` date DEFAULT NULL,
  `date_repiquage` date DEFAULT NULL,
  `date_floraison` date DEFAULT NULL,
  `date_recolte` date DEFAULT NULL,
  `quantite_recolte` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_plante`),
  KEY `id_graine_idx` (`id_graine`),
  KEY `id_jardin_idx` (`id_jardin`),
  KEY `id_zone_idx` (`id_zone`),
  KEY `id_cycle_vegetatif_idx` (`id_cycle_vegetatif`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `plante`
--

INSERT INTO `plante` (`id_plante`, `id_graine`, `id_jardin`, `id_zone`, `id_cycle_vegetatif`, `date_semis`, `date_repiquage`, `date_floraison`, `date_recolte`, `quantite_recolte`) VALUES
(5, 1, NULL, 3, 2, NULL, '2022-02-08', NULL, NULL, NULL),
(6, 2, NULL, 1, 2, '2022-04-02', '2022-05-01', NULL, NULL, NULL);

--
-- Déclencheurs `plante`
--
DROP TRIGGER IF EXISTS `Historiser_cycle_végé`;
DELIMITER $$
CREATE TRIGGER `Historiser_cycle_végé` AFTER UPDATE ON `plante` FOR EACH ROW INSERT INTO historique_cycle(date_changement, id_plante, id_cycle_vegetatif) VALUES (NOW(),OLD.id_plante, OLD.id_cycle_vegetatif)
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `Historiser_zone`;
DELIMITER $$
CREATE TRIGGER `Historiser_zone` AFTER UPDATE ON `plante` FOR EACH ROW INSERT INTO historique_zone (date_changement, id_plante, id_zone) VALUES (NOW(),OLD.id_plante, OLD.id_zone)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `plantes_hors_zone`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `plantes_hors_zone`;
CREATE TABLE IF NOT EXISTS `plantes_hors_zone` (
`plante` int(11)
,`id_graine` int(11)
,`nom` varchar(30)
,`description` longtext
,`zone` int(11)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `plantes_recap`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `plantes_recap`;
CREATE TABLE IF NOT EXISTS `plantes_recap` (
`id_graine` int(11)
,`id_famille` int(11)
,`nom` varchar(30)
,`nom_latin` varchar(50)
,`classification` enum('Vivace','Annuelle','Bisannuelle')
,`hauteur_max` int(11)
,`photo` varchar(255)
,`icone` varchar(255)
,`description` longtext
);

-- --------------------------------------------------------

--
-- Structure de la table `rusticite`
--

DROP TABLE IF EXISTS `rusticite`;
CREATE TABLE IF NOT EXISTS `rusticite` (
  `id_rusticite` int(11) NOT NULL AUTO_INCREMENT,
  `code_rusticite` varchar(2) NOT NULL,
  `temp_min` decimal(3,1) NOT NULL,
  PRIMARY KEY (`id_rusticite`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rusticite`
--

INSERT INTO `rusticite` (`id_rusticite`, `code_rusticite`, `temp_min`) VALUES
(1, '1', '-99.0'),
(2, '2', '-45.0'),
(3, '3', '-40.0'),
(4, '4', '-34.0'),
(5, '5', '-29.0'),
(6, '6', '23.0'),
(7, '7', '-18.0'),
(8, '8', '-12.0'),
(9, '8a', '-12.2'),
(10, '8b', '-9.4'),
(11, '9', '-7.0'),
(12, '9a', '-6.7'),
(13, '9b', '-3.9'),
(14, '10', '-1.0'),
(15, '11', '4.0');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(30) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `mot_de_passe` varchar(50) NOT NULL,
  PRIMARY KEY (`id_utilisateur`),
  UNIQUE KEY `mail_UNIQUE` (`mail`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `pseudo`, `mail`, `mot_de_passe`) VALUES
(1, 'toto', 'toto@toto.fr', 'f71dbe52628a3f83a77ab494817525c6'),
(2, 'igor', 'igor@toto.fr', 'dd97813dd40be87559aaefed642c3fbb');

-- --------------------------------------------------------

--
-- Structure de la table `zone`
--

DROP TABLE IF EXISTS `zone`;
CREATE TABLE IF NOT EXISTS `zone` (
  `id_zone` int(11) NOT NULL AUTO_INCREMENT,
  `id_jardin` int(11) NOT NULL,
  `id_rusticite` int(11) DEFAULT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `description` text,
  `latitude` decimal(11,0) NOT NULL,
  `longitude` decimal(11,0) NOT NULL,
  PRIMARY KEY (`id_zone`),
  KEY `id_jardin_idx` (`id_jardin`),
  KEY `id_rusticite_idx` (`id_rusticite`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `zone`
--

INSERT INTO `zone` (`id_zone`, `id_jardin`, `id_rusticite`, `nom`, `description`, `latitude`, `longitude`) VALUES
(1, 1, NULL, 'Carré potager', 'Terre du jardin, terreau et compost', '0', '0'),
(2, 1, 13, 'Veranda', NULL, '0', '0'),
(3, 2, NULL, 'Jardin partagé', 'Terre du jardin + Compost + Ammendé en 2021', '0', '0');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `zones_recap`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `zones_recap`;
CREATE TABLE IF NOT EXISTS `zones_recap` (
`id_graine` int(11)
,`id_famille` int(11)
,`nom` varchar(30)
,`nom_latin` varchar(50)
,`classification` enum('Vivace','Annuelle','Bisannuelle')
,`hauteur_max` int(11)
,`photo` varchar(255)
,`icone` varchar(255)
,`description` longtext
);

-- --------------------------------------------------------

--
-- Structure de la vue `liste_des_plantes`
--
DROP TABLE IF EXISTS `liste_des_plantes`;

DROP VIEW IF EXISTS `liste_des_plantes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `liste_des_plantes`  AS SELECT `g`.`nom` AS `Plante`, `z`.`nom` AS `Zone`, `cv`.`nom` AS `Cycle_de_vie` FROM (`cycle_vegetatif` `cv` left join (`zone` `z` left join (`graine` `g` left join `plante` `p` on((`g`.`id_graine` = `p`.`id_graine`))) on((`z`.`id_zone` = `p`.`id_zone`))) on((`cv`.`id_cycle_vegetatif` = `p`.`id_cycle_vegetatif`))) WHERE (`p`.`id_graine` = `g`.`id_graine`) ;

-- --------------------------------------------------------

--
-- Structure de la vue `plantes_hors_zone`
--
DROP TABLE IF EXISTS `plantes_hors_zone`;

DROP VIEW IF EXISTS `plantes_hors_zone`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `plantes_hors_zone`  AS SELECT `plante`.`id_plante` AS `plante`, `graine`.`id_graine` AS `id_graine`, `graine`.`nom` AS `nom`, `graine`.`description` AS `description`, `plante`.`id_zone` AS `zone` FROM (`graine` join `plante`) WHERE isnull(`plante`.`id_zone`) ;

-- --------------------------------------------------------

--
-- Structure de la vue `plantes_recap`
--
DROP TABLE IF EXISTS `plantes_recap`;

DROP VIEW IF EXISTS `plantes_recap`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `plantes_recap`  AS SELECT `graine`.`id_graine` AS `id_graine`, `graine`.`id_famille` AS `id_famille`, `graine`.`nom` AS `nom`, `graine`.`nom_latin` AS `nom_latin`, `graine`.`classification` AS `classification`, `graine`.`hauteur_max` AS `hauteur_max`, `graine`.`photo` AS `photo`, `graine`.`icone` AS `icone`, `graine`.`description` AS `description` FROM `graine` ;

-- --------------------------------------------------------

--
-- Structure de la vue `zones_recap`
--
DROP TABLE IF EXISTS `zones_recap`;

DROP VIEW IF EXISTS `zones_recap`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `zones_recap`  AS SELECT `graine`.`id_graine` AS `id_graine`, `graine`.`id_famille` AS `id_famille`, `graine`.`nom` AS `nom`, `graine`.`nom_latin` AS `nom_latin`, `graine`.`classification` AS `classification`, `graine`.`hauteur_max` AS `hauteur_max`, `graine`.`photo` AS `photo`, `graine`.`icone` AS `icone`, `graine`.`description` AS `description` FROM `graine` ;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `etat_plante`
--
ALTER TABLE `etat_plante`
  ADD CONSTRAINT `fk_id_etat_etat_plante` FOREIGN KEY (`id_etat`) REFERENCES `etat` (`id_etat`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_id_plante_etat_plante` FOREIGN KEY (`id_plante`) REFERENCES `plante` (`id_plante`) ON DELETE CASCADE;

--
-- Contraintes pour la table `famille`
--
ALTER TABLE `famille`
  ADD CONSTRAINT `fk_id_famille_parente_famille` FOREIGN KEY (`id_famille_parente`) REFERENCES `famille` (`id_famille`) ON DELETE CASCADE;

--
-- Contraintes pour la table `graine`
--
ALTER TABLE `graine`
  ADD CONSTRAINT `fk_id_famille_graine` FOREIGN KEY (`id_famille`) REFERENCES `famille` (`id_famille`);

--
-- Contraintes pour la table `historique_cycle`
--
ALTER TABLE `historique_cycle`
  ADD CONSTRAINT `fk_id_cycle_vegetatif_historique_cycle` FOREIGN KEY (`id_cycle_vegetatif`) REFERENCES `cycle_vegetatif` (`id_cycle_vegetatif`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_id_plante_historique_cycle` FOREIGN KEY (`id_plante`) REFERENCES `plante` (`id_plante`) ON DELETE CASCADE;

--
-- Contraintes pour la table `historique_etat`
--
ALTER TABLE `historique_etat`
  ADD CONSTRAINT `fk_id_etat_historique_etat` FOREIGN KEY (`id_etat`) REFERENCES `etat` (`id_etat`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_id_plante_historique_etat` FOREIGN KEY (`id_plante`) REFERENCES `plante` (`id_plante`) ON DELETE CASCADE;

--
-- Contraintes pour la table `historique_zone`
--
ALTER TABLE `historique_zone`
  ADD CONSTRAINT `fk_id_zone_historique_zone` FOREIGN KEY (`id_zone`) REFERENCES `zone` (`id_zone`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_idplante_historique_zone` FOREIGN KEY (`id_plante`) REFERENCES `plante` (`id_plante`) ON DELETE CASCADE;

--
-- Contraintes pour la table `jardin`
--
ALTER TABLE `jardin`
  ADD CONSTRAINT `fk_id_rusticite_jardin` FOREIGN KEY (`id_rusticite`) REFERENCES `rusticite` (`id_rusticite`);

--
-- Contraintes pour la table `jardin_utilisateur`
--
ALTER TABLE `jardin_utilisateur`
  ADD CONSTRAINT `fk_id_jardin_jardin_utilisateir` FOREIGN KEY (`id_jardin`) REFERENCES `jardin` (`id_jardin`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_id_utilisateur_jardin_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `plante`
--
ALTER TABLE `plante`
  ADD CONSTRAINT `fk_cycle_vegetatif_plante` FOREIGN KEY (`id_cycle_vegetatif`) REFERENCES `cycle_vegetatif` (`id_cycle_vegetatif`),
  ADD CONSTRAINT `fk_id_graine_plante` FOREIGN KEY (`id_graine`) REFERENCES `graine` (`id_graine`),
  ADD CONSTRAINT `fk_jardin_plante` FOREIGN KEY (`id_jardin`) REFERENCES `jardin` (`id_jardin`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_zone_plante` FOREIGN KEY (`id_zone`) REFERENCES `zone` (`id_zone`);

--
-- Contraintes pour la table `zone`
--
ALTER TABLE `zone`
  ADD CONSTRAINT `fk_id_jardin_zone` FOREIGN KEY (`id_jardin`) REFERENCES `jardin` (`id_jardin`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_id_rusticite_zone` FOREIGN KEY (`id_rusticite`) REFERENCES `rusticite` (`id_rusticite`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
